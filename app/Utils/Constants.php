<?php

namespace App\Utils;

class Constants
{
    const PER_PAGE = 20;
    const LIMIT_MESSAGES_PER_DAY = 5;
}
