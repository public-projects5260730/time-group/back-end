<?php

namespace App\Observers;

use App\Models\Post;
use App\Services\SEO\SeoTags;
use Illuminate\Support\Arr;
use SEO\Models\MetaTag;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Str;

class PostObserver
{
    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        $this->updateSeoTags($post);
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        $this->updateSeoTags($post);
    }

    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        SeoTags::deleteTagsByModel($post);
    }

    protected function updateSeoTags(Post $post)
    {
        $requestData = request()->only(['page', 'meta']);
        if (empty($requestData['page']) || empty($requestData['meta'])) {
            return;
        }

        if (is_array($requestData['meta'])) {
            $seoMetaTags = MetaTag::query()->findMany(array_keys($requestData['meta']), ['id', 'name', 'property'])->keyBy('id');
        }

        $imageUrl = Voyager::image($post->image);

        $url = url("posts/{$post->slug}/view");

        $title = Str::limit($post->title, 85, '...');
        if ($post->seo_title) {
            $title = Str::limit($post->seo_title, 85, '...');
        }

        $description = $post->title;
        if ($post->meta_description) {
            $description = $post->meta_description;
        } elseif ($post->excerpt) {
            $description = $post->excerpt;
        }

        if (!empty($description)) {
            $description = Str::limit($description, 165, '...');
        }


        $keywords = '';
        if ($post->meta_keywords) {
            $keywords = $post->meta_keywords;
        }

        if (!empty($keywords)) {
            $keywords = Str::limit($keywords, 240, '...');
            $keywordsArr = preg_split('/\s*[,;]+\s*/', $keywords, -1, PREG_SPLIT_NO_EMPTY);
            $keywords = implode(', ', $keywordsArr);
        }


        foreach ($requestData as $type => &$data) {
            if (!is_array($data)) {
                continue;
            }
            if ($type == 'meta' && empty($seoMetaTags)) {
                continue;
            }

            foreach ($data as $field => &$value) {
                if ($type == 'meta') {
                    $meta = $seoMetaTags->offsetGet($field);
                    if (empty($meta)) {
                        continue;
                    }
                    if (!empty($meta->name)) {
                        $field = $meta->name;
                    } else if (!empty($meta->property)) {
                        $field = $meta->property;
                    } else {
                        continue;
                    }
                }

                if (is_string($field)) {
                    if (preg_match('/title$/iu', $field, $match)) {
                        $value = $title;
                    } else if (preg_match('/description$/iu', $field, $match)) {
                        $value = $description;
                    } else if (preg_match('/keyword$/iu', $field, $match)) {
                        $value = $keywords;
                    } else if (preg_match('/keywords$/iu', $field, $match)) {
                        $value = $keywords;
                    } else if (preg_match('/author$/iu', $field, $match)) {
                        if (empty($value)) {
                            $value = $post->author->name;
                        }
                    } elseif ($field == 'robot_index' && empty($value)) {
                        $value = 'index';
                    }
                }
            }
        }

        $data = Arr::get($requestData, 'page', []);
        $data['images'] =  [$imageUrl];

        SeoTags::save(
            $post,
            $url,
            $data
        );
    }
}
