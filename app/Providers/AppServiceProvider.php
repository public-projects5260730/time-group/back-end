<?php

namespace App\Providers;

use App\Helpers\DataFilterer;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(public_path(), 'public');
        $this->loadViewsFrom(resource_path('views/admin'), 'admin');

        $this->app->singleton('DataFilterer', function () {
            return new DataFilterer();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!!env('USE_HTTPS', false)) {
            URL::forceScheme('https');
        }
    }
}
