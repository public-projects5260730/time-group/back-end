<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class PostController extends VoyagerBaseController
{
    protected $slug = 'posts';

    public function category(Request $request, $slug)
    {
        $request->merge([
            'categories' => [
                'slug' => $slug
            ]
        ]);

        return parent::index($request);
    }

    public function create(Request $request)
    {
        $response = parent::create($request);
        $responseData = $response->getData();

        if (view()->exists("admin::posts.edit-add")) {
            return Voyager::view('admin::posts.edit-add', $responseData);
        }

        return $response;
    }

    public function edit(Request $request, $id)
    {
        $response = parent::edit($request, $id);
        $responseData = $response->getData();

        if (view()->exists("admin::posts.edit-add")) {
            return Voyager::view('admin::posts.edit-add', $responseData);
        }
        return $response;
    }

    public function store(Request $request)
    {
        $response = parent::store($request);
        if ($response instanceof RedirectResponse) {
            $previousUrl = $request->get('previousUrl');
            if (!empty($previousUrl) && preg_match('#\/category$#i', $previousUrl, $match)) {
                return redirect($previousUrl);
            }
        }

        return $response;
    }

    public function update(Request $request, $id)
    {
        $request->merge(['ignoreFilters' => true]);

        $response = parent::update($request, $id);
        if ($response instanceof RedirectResponse) {
            $previousUrl = $request->get('previousUrl');
            if (!empty($previousUrl) && preg_match('#\/category$#i', $previousUrl, $match)) {
                return redirect($previousUrl);
            }
        }

        return $response;
    }

    public function destroy(Request $request, $id)
    {
        parent::destroy($request, $id);
        return redirect()->back();
    }
}
