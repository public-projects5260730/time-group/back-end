<?php

namespace App\Http\Controllers\Api;

use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function index(Request $request)
    {
        $query = Banner::query()->filters()->ordered();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
        }

        $banners = $query->get();

        return $this->response(compact('banners'));
    }
}
