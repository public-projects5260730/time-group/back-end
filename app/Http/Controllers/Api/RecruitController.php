<?php

namespace App\Http\Controllers\Api;

use App\Models\Candidate;
use App\Models\Recruit;
use App\Utils\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RecruitController extends Controller
{
    public function index(Request $request)
    {
        $query = Recruit::query()
            ->withCasts(['expired_at' => 'timestamp'])
            ->filters();
        $query->casts();
        $query->published();
        $query->ordered();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
            $recruits = $query->get();
            return $this->response(compact('recruits'));
        }

        $paginate = $query->paginate($request->get('perPage', Constants::PER_PAGE));
        return $this->response(compact('paginate'));
    }

    public function options()
    {
        /** @var \Illuminate\Database\Eloquent\Collection $recruits  */
        $recruits = Recruit::query()->published()->get(['location', 'specialisms'])->toArray();

        $locationOptions = $specialismsOptions = [];
        foreach ($recruits as $recruit) {
            if (!empty($recruit['specialisms'])) {
                $options = Str::split($recruit['specialisms']);
                $specialismsOptions = array_merge($options, $specialismsOptions);
            }

            if (!empty($recruit['location'])) {
                $locations = array_reverse(Str::split($recruit['location']));
                foreach ($locations as $location) {
                    if (Str::slug($location) == 'viet-nam') {
                        continue;
                    }
                    $locationOptions[] = $location;
                    break;
                }
            }
        }

        $locationOptions = array_unique($locationOptions);
        $specialismsOptions = array_unique($specialismsOptions);
        sort($locationOptions);
        sort($specialismsOptions);

        return $this->response(compact('locationOptions', 'specialismsOptions'));
    }

    public function sendCv(Request $request, int $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'string',
            'fullName' => 'required|string',
            'cvFile' => 'mimes:doc,docx,pdf|max:5120',
        ]);

        if ($validator->fails()) {
            return $this->errors($validator->errors()->messages());
        }

        $recruit = Recruit::query()->published()->findOrFail($id);

        $validated = $validator->validated();
        $inputData = filter($validated, [
            'email' => 'str',
            'phone' => 'str',
            'fullName' => 'str',
        ], true);


        $candidate = Candidate::query()
            ->where('recruit_id', $recruit->getKey())
            ->where('email', $inputData['email'])
            ->firstOrNew();

        $inputData['full_name'] = $inputData['fullName'];
        unset($inputData['fullName']);

        $candidate->ip_address = $request->ip();
        if (!$candidate->exists) {
            $candidate->recruit_id = $recruit->getKey();
            $candidate->cv_file = '';
        }


        $candidate->mergeFillable(array_keys($inputData))
            ->fill($inputData)
            ->save();

        if ($request->hasFile('cvFile')) {
            if ($candidate->cv_file) {
                Storage::disk(config('voyager.storage.disk', 'public'))->delete($candidate->cv_file);
            }

            $file = $request->file('cvFile');
            $fileName = sprintf('%s.%s', Str::slug($candidate->email), $file->getClientOriginalExtension());
            $file->storeAs('candidates/' . $candidate->getKey(), $fileName, config('voyager.storage.disk', 'public'));

            $cfFileName = sprintf('candidates/%s/%s', $candidate->getKey(), $fileName);
            $candidate->cv_file = $cfFileName;
            $candidate->saveQuietly();
        }
        return $this->successfully();
    }
}
