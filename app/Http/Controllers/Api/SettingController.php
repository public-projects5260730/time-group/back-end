<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use TCG\Voyager\Models\Setting;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        $settings = Setting::query()->where('group', 'Site')->pluck('value', 'key')->toArray();
        $settings = Arr::undot($settings);
        if (!empty($settings['site'])) {
            $settings = $settings['site'];
        }
        return $this->response(compact('settings'));
    }
}
