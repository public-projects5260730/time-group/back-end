<?php

namespace App\Http\Controllers\Api;

use App\Models\Company;
use App\Models\CompanyGroup;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        $companies = Company::query()->casts()->ordered()->get();
        return $this->response(compact('companies'));
    }

    public function view(Request $request, int $id)
    {
        $company = Company::query()->casts()->findOrFail($id);
        return $this->response(compact('company'));
    }

    public function groups(Request $request)
    {
        $companyGroups = CompanyGroup::query()
            ->with('companies')
            ->casts()
            ->ordered()
            ->get();

        return $this->response(compact('companyGroups'));
    }
}
