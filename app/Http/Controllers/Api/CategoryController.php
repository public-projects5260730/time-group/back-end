<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Utils\Constants;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $query = Category::query()->filters();
        $query->casts();
        $query->ordered();

        $categories = $query->get();

        return $this->response(compact('categories'));
    }

    public function view(Request $request, $id)
    {
        $category = Category::query()->filters(['id' => $id])->casts()->firstOrFail();
        return $this->response(compact('category'));
    }

    public function posts(Request $request, $id)
    {
        $filters = ['id' => $id] + $request->all();
        $category = Category::query()
            ->with(['posts' => function ($query) use ($request) {
                if ($request->exists('limit')) {
                    $query->limit($request->get('limit'));
                }
                $query->casts();
                $query->published();
                $query->ordered();
            }])
            ->filters($filters)
            ->casts()
            ->firstOrFail();

        return $this->response(compact('category'));
    }
}
