<?php

namespace App\Http\Controllers\Api;

use App\Models\Personnel;
use Illuminate\Http\Request;

class PersonnelController extends Controller
{
    public function index(Request $request)
    {
        $query = Personnel::query()->ordered();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
        }

        $personnels = $query->get();

        return $this->response(compact('personnels'));
    }
}
