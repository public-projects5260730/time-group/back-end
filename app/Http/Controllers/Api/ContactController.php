<?php

namespace App\Http\Controllers\Api;

use App\Models\ContactMessage;
use App\Utils\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{

    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->errors($validator->errors()->messages());
        }

        $validated = $validator->validated();
        $inputData = filter($validated, [
            'email' => 'str',
            'message' => 'str',
        ], true);

        $messageCount = ContactMessage::query()
            ->where('ip_address', $request->ip())
            ->whereDate('created_at', Carbon::today())
            ->count();

        if ($messageCount >= Constants::LIMIT_MESSAGES_PER_DAY) {
            return $this->error('You have reached your message limit for today.');
        }

        $inputData += [
            'ip_address' => $request->ip()
        ];

        $message = new ContactMessage();
        $message->mergeFillable(array_keys($inputData))
            ->fill($inputData)
            ->save();

        return $this->successfully();
    }
    //
}
