<?php

namespace App\Http\Controllers\Api;

use App\Services\SEO\SeoTags;
use Illuminate\Http\Request;

class SeoTagController extends Controller
{
    public function index(Request $request)
    {
        $uri = $request->get('uri');
        $cloneRequest = $request->duplicate();
        $cloneRequest->server->set('REQUEST_URI', $uri);

        $seoTagsObj = new SeoTags($cloneRequest);
        $seoTags = $seoTagsObj->tags();
        return $this->response(compact('seoTags'));
    }
}
