<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use App\Utils\Constants;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $query = Post::query()->filters();
        $query->casts();
        $query->published();
        $query->ordered();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
            $posts = $query->get();
            return $this->response(compact('posts'));
        }

        $paginate = $query->paginate($request->get('perPage', Constants::PER_PAGE));
        return $this->response(compact('paginate'));
    }

    public function view(Request $request, $slug)
    {
        $post = Post::query()->casts()->filters(['slug' => $slug])->published()->firstOrFail();
        return $this->response(compact('post'));
    }

    public function related(Request $request, $slug)
    {
        $post = Post::query()->with('categories')->filters(['slug' => $slug])->published()->firstOrFail();

        /** @var \Illuminate\Database\Eloquent\Collection $categories */
        $categories = $post->categories;
        $categoryIds =  $categories->keyBy('id')->keys()->toArray();
        if (empty($categoryIds)) {
            $posts = [];
            return $this->response(compact('posts'));
        }

        $query = Post::query()->filters([
            '__notIds' => $post->getKey(),
            'categories' => [
                'id' => $categoryIds
            ]
        ]);
        $query->casts();
        $query->published();
        $query->ordered();

        $query->limit($request->get('limit', 4));
        $posts = $query->get();
        return $this->response(compact('posts'));
    }
}
