<?php

namespace App\Models;

use App\Models\Traits\Filters;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory, Filters;

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    public function prepareFilter($query, $field, $condition)
    {
        switch ($field) {
            case 'id':
                if (!is_array($condition)) {
                    $query->where($field, $condition);
                } else {
                    $query->whereIn($field, $condition);
                }
                break;

            case 'positions':
                if (!is_array($condition)) {
                    $query->where($field, 'LIKE', '%"' . $condition . '"%');
                } else {
                    $query->where(function ($query) use ($field, $condition) {
                        foreach ($condition as $position) {
                            $query->orWhere($field, 'LIKE', '%"' . $position . '"%');
                        }
                    });
                }
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'id' => 'uint|list-uint|array-uint,nullable',
            'positions' => 'list-str|array-str,nullable',
        ];
    }
}
