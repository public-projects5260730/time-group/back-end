<?php

namespace App\Models;

use App\Models\Traits\Casts;
use App\Models\Traits\Filters;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Recruit extends Model
{
    use HasFactory, Casts, Filters;

    protected $table = 'recruit';

    public function scopePublished(Builder $query)
    {
        return $query->whereDate('expired_at', '>=', Carbon::today());
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function prepareFilter(Builder $query, $field, $condition)
    {
        switch ($field) {
            case 'id':
                if (!is_array($condition)) {
                    $query->where($field, $condition);
                } else {
                    $query->whereIn($field, $condition);
                }
                break;

            case 'search':
                $query->where(function ($query) use ($condition) {
                    $fields = ['title', 'body', 'location', 'position', 'specialisms'];
                    foreach ($fields as $field) {
                        $query->orWhere($field, 'LIKE', '%' . $condition . '%');
                    }
                });

            case 'specialisms':
            case 'location':
                if (!is_array($condition)) {
                    $query->where($field, 'LIKE', '%' . $condition . '%');
                } else {
                    $query->where(function ($query) use ($field, $condition) {
                        foreach ($condition as $value) {
                            $query->orWhere($field, 'LIKE', '%' . $value . '%');
                        }
                    });
                }
                break;

                $query->where(function ($query) use ($condition) {
                    $fields = ['title', 'body', 'location', 'position', 'specialisms'];
                    foreach ($fields as $field) {
                        $query->orWhere($field, 'LIKE', '%' . $condition . '%');
                    }
                });
        }
    }

    public function getFilterRules()
    {
        return [
            'search' => 'str,nullable',
            'specialisms' => 'str|list-str|array-str,nullable',
            'location' => 'str|list-str|array-str,nullable',
        ];
    }
}
