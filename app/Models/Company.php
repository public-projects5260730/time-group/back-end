<?php

namespace App\Models;

use App\Models\Traits\Casts;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory, Casts;

    protected $table = 'companies';

    public function scopeOrdered($query)
    {
        $query
            ->orderBy('order', 'ASC')
            ->orderBy('created_at', 'DESC');
    }
}
