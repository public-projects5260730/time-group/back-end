<?php

namespace App\Models;

use App\Models\Traits\Casts;
use App\Models\Traits\Filters;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use TCG\Voyager\Models\Category as ModelsCategory;

class Category extends ModelsCategory
{
    use HasFactory, Casts, Filters;

    public function posts()
    {
        return $this->belongsToMany(Post::class)
            ->casts()
            ->published()
            ->ordered();
    }

    public function scopeOrdered($query)
    {
        $query
            ->orderBy('order', 'ASC')
            ->orderBy('created_at', 'DESC');
    }

    public function prepareFilter($query, $field, $condition)
    {
        switch ($field) {
            case 'id':
            case 'slug':
                if (!is_array($condition)) {
                    $query->where($field, $condition);
                } else {
                    $query->whereIn($field, $condition);
                }
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'posts' => 'array,nullable',
            'id' => 'uint|list-uint|array-uint,nullable',
            'slug' => 'str|list-str|array-str,nullable',
        ];
    }
}
