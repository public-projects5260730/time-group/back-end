<?php

namespace App\Models;

use App\Models\Traits\Casts;
use App\Models\Traits\Filters;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use TCG\Voyager\Models\Post as ModelsPost;

class Post extends ModelsPost
{
    use HasFactory, Casts, Filters;

    public function category()
    {
        return;
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function prepareFilter(Builder $query, $field, $condition)
    {
        switch ($field) {
            case 'id':
            case 'slug':
                if (!is_array($condition)) {
                    $query->where($field, $condition);
                } else {
                    $query->whereIn($field, $condition);
                }
                break;

            case '__notIds':
                if (!is_array($condition)) {
                    $query->where('id', '!=', $condition);
                } else {
                    $query->whereNotIn('id', $condition);
                }
                break;

            case 'featured':
                $query->where($field, (bool) $condition);
                break;

            case 'search':
                $query->where(function ($query) use ($condition) {
                    $fields = ['title', 'seo_title', 'excerpt', 'body', 'meta_description', 'meta_keywords'];
                    foreach ($fields as $field) {
                        $query->orWhere($field, 'LIKE', '%' . $condition . '%');
                    }
                });
        }
    }

    public function getFilterRules()
    {
        if (request('ignoreFilters', false)) {
            return [];
        }

        return [
            'categories' => 'array,nullable',

            'id' => 'uint|list-uint|array-uint,nullable',
            'slug' => 'str|list-str|array-str,nullable',
            'featured' => 'bool,nullable',
            'search' => 'str,nullable',
            '__notIds' => 'uint|list-uint|array-uint,nullable',
        ];
    }
}
