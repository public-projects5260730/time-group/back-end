<?php

namespace App\Models;

use App\Models\Traits\Casts;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyGroup extends Model
{
    use HasFactory, Casts;

    protected $table = 'company_groups';

    public function companies()
    {
        return $this->hasMany(Company::class)->casts()->ordered();
    }

    public function scopeOrdered($query)
    {
        $query
            ->orderBy('order', 'ASC')
            ->orderBy('created_at', 'DESC');
    }
}
