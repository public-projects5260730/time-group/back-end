<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Filters
{
    public function scopeFilters(Builder $query, array $filters = null)
    {
        $filters = $this->getFilters($filters);

        foreach ($filters as $field => $condition) {
            $this->prepareFilter($query, $field, $condition);
            $this->relationFilters($query, $field, $condition);
        }
    }

    protected function relationFilters(Builder $query, $relation, $conditions)
    {
        if (!is_array($conditions) || !$this->isRelation($relation)) {
            return;
        }

        $query->whereHas($relation, function (Builder $query) use ($conditions) {
            if ($query->hasNamedScope('filters')) {
                $query->filters($conditions);
            }
        });
    }

    protected function prepareFilter(Builder $query, $field, $condition)
    {
    }

    protected function getFilters(array $filters = null)
    {
        $rules = $this->getFilterRules();
        if (empty($rules) || !is_array($rules)) {
            return [];
        }

        if ($filters === null) {
            $filters = request()->only(array_keys($rules));
        }

        return filter($filters, $rules);
    }

    protected function getFilterRules()
    {
        return [];
    }
}
