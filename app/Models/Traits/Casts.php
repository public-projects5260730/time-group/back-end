<?php

namespace App\Models\Traits;


trait Casts
{
    public function scopeCasts($query, array $casts = null)
    {
        if ($casts === null) {
            $casts = [
                'created_at' => 'timestamp',
                'updated_at' => 'timestamp',
            ];
        }
        $this->casts = $casts + $this->casts;
    }
}
