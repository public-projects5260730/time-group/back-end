<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    use HasFactory;

    public function scopeOrdered($query)
    {
        $query
            ->orderBy('order', 'ASC')
            ->orderBy('created_at', 'DESC');
    }
}
