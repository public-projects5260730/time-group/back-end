<?php

namespace App\Services\SEO;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use SEO\Models\Page;
use SEO\Seo;
use SEO\Tag;

class SeoTags extends Seo
{
    public function __construct(Request $request = null)
    {
        if ($request === null) {
            return parent::__construct();
        }

        $this->request = $request;
        $path = $this->request->path();

        $this->page = Page::whereIn('path', [trim($path, "/"), "/" . $path, url($path)])->first();

        if ($this->page) {

            if (config('seo.cache.driver') == 'file') {
                $this->filePath = rtrim(config('seo.cache.storage'), "/") . '/' . $this->page->id . '.html';

                if (file_exists($this->filePath)) {
                    $this->splFileObject = new \SplFileObject($this->filePath, 'r');
                }
            }
        }
    }

    public static function saveSeoPage(Model $model, $url, $requestData = [], $data = [])
    {
        try {
            $images = [];
            $fillable = Arr::get($requestData, 'page');

            if (isset($data['images'])) {
                $images = $data['images'];
                unset($data['images']);
            }

            /** @var Page $page */
            $page = Page::firstOrNew([
                'object' => get_class($model),
                'object_id' => $model->getKey(),
            ]);

            $page->path = $url;
            $page->object = get_class($model);
            $page->object_id = $model->getKey();

            foreach ($fillable as $column => $value) {
                if (empty($value) && isset($data[$column]) && !empty($data[$column])) {
                    $fillable[$column] = $data[$column];
                }
            }
            $page->fill($fillable);
            if ($page->save()) {
                if (!empty($images)) {
                    $page->destroyImages()->saveImagesFromArray($images);
                }
                $metaValues = Arr::get($requestData, 'meta');

                $page->saveMeta($metaValues);

                $metaFiles = request()->file('meta');
                if (!empty($metaFiles) && is_array($metaFiles)) {
                    $page->saveMeta(static::upload($metaFiles));
                }

                // Its time to refresh cache or making new cache
                $tag = new Tag($page);
                $tag->make()->save();
            }
            return $page;
        } catch (\Exception $e) {
            Log::error($e->getMessage() . 'on Line ' . $e->getLine() . ' in ' . $e->getFile());
        }
    }

    public static function deleteTagsByModel(Model $model)
    {
        /** @var Page $page */
        Page::query()
            ->where('object', get_class($model))
            ->where('object_id', $model->getKey())
            ->delete();
    }
}
