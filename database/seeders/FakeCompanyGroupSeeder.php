<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CompanyGroup;

class FakeCompanyGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'id' => 1,
                'name' => 'Công Nghệ',
            ],
            [
                'id' => 2,
                'name' => 'Công nghệ - Maketing',
            ],
            [
                'id' => 3,
                'name' => 'Sáng tạo nội dung',
            ],
            [
                'id' => 4,
                'name' => 'Pháp chế - đào tạo',
            ],
        ];

        foreach ($groups as $group) {
            $result = CompanyGroup::query()->findOrNew($group['id']);
            if ($result->exists) {
                continue;
            }

            $result->fill([
                'name'            => $group['name'],
                'order'        => $group['id'] * 10,
            ])->save();
        }
    }
}
