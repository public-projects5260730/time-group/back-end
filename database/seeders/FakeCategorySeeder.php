<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use Illuminate\Support\Str;

class FakeCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNames  = [
            'Tin Tức',
            'Khám Phá Tin Tức',
        ];

        foreach ($categoryNames as $index => $name) {
            $slug = Str::slug($name);
            $category = Category::query()->firstOrNew(['slug' => $slug]);
            if ($category->exists) {
                continue;
            }

            $category->fill([
                'name' => $name,
                'slug' => $slug,
                'order' => ($index + 1) * 10
            ]);
            $category->save();
        }
    }
}
