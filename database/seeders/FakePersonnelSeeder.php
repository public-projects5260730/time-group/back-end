<?php

namespace Database\Seeders;

use App\Models\Personnel;
use Illuminate\Database\Seeder;

class FakePersonnelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            $name = 'Kirsten Davies -- ' . $i;

            $result = Personnel::query()->firstOrNew(['name' => $name]);
            if ($result->exists) {
                continue;
            }
            $imgId = $i;
            if ($imgId > 6) {
                $imgId = random_int(1, 6);
            }
            $result->fill([
                'name' => $name,
                'position' => 'SVP & CISO',
                'image' => "personnels/personnel-{$imgId}.png",
                'order' => $i * 10
            ])->save();
        }
    }
}
