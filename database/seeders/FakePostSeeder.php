<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class FakePostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->newPosts();
        $this->newPosts2();
    }

    protected function newPosts()
    {
        $titles = [
            'STRONG LEADERSHIP',
            'QUALIFIED RELATIONSHIP',
            'INNOVATIVE TECHNOLOGY',
        ];
        for ($i = 1; $i <= 10; $i++) {
            $title = ucwords(strtolower(Arr::random($titles)));
            $slug = Str::slug($title) . '--' . $i;

            $post = Post::query()->firstOrNew(['slug' => $slug]);
            if ($post->exists) {
                continue;
            }

            $post->fill([
                'title'            => $title,
                'author_id'        => 0,
                'seo_title'        => null,
                'excerpt'   => 'Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nongfdummy nibh euismod',
                'body'      => $this->postContent(),
                'image'            => "posts/tin-tuc-default.png",
                'slug'             => $slug,
                'meta_description' => 'This is the meta description',
                'meta_keywords'    => 'keyword1, keyword2, keyword3',
                'status'           => 'PUBLISHED',
                'featured'         => 0,
            ])->save();

            $post->categories()->sync([1]);
        }
    }

    protected function newPosts2()
    {
        $title = 'CES23 - JBL trình làng mâm đĩa than Spinner BT: MM phono stage, hỗ trợ Bluetooth aptX HD, giá $399';
        $slug = Str::slug($title);

        for ($i = 1; $i <= 40; $i++) {
            $slug .= '--' . $i;
            $post = Post::query()->firstOrNew(['slug' => $slug]);
            if ($post->exists) {
                continue;
            }

            $post->fill([
                'title'            => $title,
                'author_id'        => 0,
                'seo_title'        => null,
                'excerpt'   => 'Google đang tiếp tục với chiến dịch #GetTheMessage nhằm thuyết phục Apple áp dụng giao thức nhắn tin RCS, lần này là',
                'body'      => $this->postContent(),
                'image'            => "posts/kham-pha-tin-tuc-default.png",
                'slug'             => $slug,
                'meta_description' => 'This is the meta description',
                'meta_keywords'    => 'keyword1, keyword2, keyword3',
                'status'           => 'PUBLISHED',
                'featured'         => $i <= 10,
            ])->save();

            $post->categories()->sync([1, 2]);
        }
    }

    protected function postContent()
    {
        $appUrl = config('app.url');
        return '<p><img src="' . $appUrl . '/storage/posts/post-view-default.png" alt="" width="100%" height="100%"></p>
        <p>2022&rsquo;s declines for the three major U.S. stock indexes were the biggest since the 2008 financial crisis, driven largely by a rout in growth stocks amid worries the Federal Reserve&rsquo;s aggressive interest rate hikes would slow earnings growth.<br>Growth stocks were under pressure for much of last year and largely underperformed economically-sensitive value stocks, breaking a trend that has lasted for most of the last decade.<br>Apple (AAPL), Alphabet (GOOGL), Microsoft (MSFT), Nvidia (NVDA), Amazon (AMZN) and Tesla (TSLA) shares were among the worst drags on the S&amp;P 500 Growth Index, with shares down between 28% and 66% last year.<br>The S&amp;P 500 Growth Index fell about 30.1% last year, while the S&amp;P 500 Value Index was down about 7.4%, with investors preferring high dividend-yielding sectors with steady earnings like energy. The energy sector recorded the biggest gains of any sector, adding 59% as oil prices surged.2022&rsquo;s declines for the three major U.S. stock indexes were the biggest since the 2008 financial crisis, driven largely by a rout in growth stocks amid worries the Federal Reserve&rsquo;s aggressive interest rate hikes would slow earnings growth.<br>Growth stocks were under pressure for much of last year and largely underperformed economically-sensitive value stocks, breaking a trend that has lasted for most of the last decade.<br>Apple (AAPL), Alphabet (GOOGL), Microsoft (MSFT), Nvidia (NVDA), Amazon (AMZN) and Tesla (TSLA) shares were among the worst drags on the S&amp;P 500 Growth Index, with shares down between 28% and 66% last year.<br>The S&amp;P 500 Growth Index fell about 30.1% last year, while the S&amp;P 500 Value Index was down about 7.4%, with investors preferring high dividend-yielding sectors with steady earnings like energy. The energy sector recorded the biggest gains of any sector, adding 59% as oil prices surged.2022&rsquo;s declines for the three major U.S. stock indexes were the biggest since the 2008 financial crisis, driven largely by a rout in growth stocks amid worries the Federal Reserve&rsquo;s aggressive interest rate hikes would slow earnings growth.<br>Growth stocks were under pressure for much of last year and largely underperformed economically-sensitive value stocks, breaking a trend that has lasted for most of the last decade.<br>Apple (AAPL), Alphabet (GOOGL), Microsoft (MSFT), Nvidia (NVDA), Amazon (AMZN) and Tesla (TSLA) shares were among the worst drags on the S&amp;P 500 Growth Index, with shares down between 28% and 66% last year.<br>The S&amp;P 500 Growth Index fell about 30.1% last year, while the S&amp;P 500 Value Index was down about 7.4%, with investors preferring high dividend-yielding sectors with steady earnings like energy. The energy sector recorded the biggest gains of any sector, adding 59% as oil prices surged.2022&rsquo;s declines for the three major U.S. stock indexes were the biggest since the 2008 financial crisis, driven largely by a rout in growth stocks amid worries the Federal Reserve&rsquo;s aggressive interest rate hikes would slow earnings growth.<br>Growth stocks were under pressure for much of last year and largely underperformed economically-sensitive value stocks, breaking a trend that has lasted for most of the last decade.<br>Apple (AAPL), Alphabet (GOOGL), Microsoft (MSFT), Nvidia (NVDA), Amazon (AMZN) and Tesla (TSLA) shares were among the worst drags on the S&amp;P 500 Growth Index, with shares down between 28% and 66% last year.<br>The S&amp;P 500 Growth Index fell about 30.1% last year, while the S&amp;P 500 Value Index was down about 7.4%, with investors preferring high dividend-yielding sectors with steady earnings like energy. The energy sector recorded the biggest gains of any sector, adding 59% as oil prices surged.2022&rsquo;s declines for the three major U.S. stock indexes were the biggest since the 2008 financial crisis, driven largely by a rout in growth stocks amid worries the Federal Reserve&rsquo;s aggressive interest rate hikes would slow earnings growth.<br>Growth stocks were under pressure for much of last year and largely underperformed economically-sensitive value stocks, breaking a trend that has lasted for most of the last decade.<br>Apple (AAPL), Alphabet (GOOGL), Microsoft (MSFT), Nvidia (NVDA), Amazon (AMZN) and Tesla (TSLA) shares were among the worst drags on the S&amp;P 500 Growth Index, with shares down between 28% and 66% last year.<br>The S&amp;P 500 Growth Index fell about 30.1% last year, while the S&amp;P 500 Value Index was down about 7.4%, with investors preferring high dividend-yielding sectors with steady earnings like energy. The energy sector recorded the biggest gains of any sector, adding 59% as oil prices surged.</p>';
    }
}
