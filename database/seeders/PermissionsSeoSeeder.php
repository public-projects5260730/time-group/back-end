<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;

class PermissionsSeoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'seos';
        Permission::firstOrCreate(['key' => 'management_' . $tableName, 'table_name' => $tableName]);
    }
}
