<?php

namespace Database\Seeders;

use App\Models\Recruit;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class FakeRecruitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles  = [
            'Content Writer',
            'Business Development (Blockchain project)',
            'Delivery Manager – Working In Dubai',
            'C++ Developer',
            'Chuyên viên pháp chế',
            'Giám đốc điều hành (Đơn vị Thành viên)',
        ];

        $workTypes = [
            'Full time',
            'Part time'
        ];

        $locations = [
            'Hà Nội, Việt Nam',
            'TP Hồ Chí Minh, Việt Nam',
            'TP Đà Nẵng, Việt Nam',
            'Hải Phòng, Việt Nam',
            'Huế, Việt Nam',
            'Vũng Tàu',
        ];

        $specialisms = [
            'developer',
            'designer',
            'writer',
            'nhân sự',
            'kế toán',
        ];

        $payRates = [
            'cạnh tranh',
            'up to 35.000.000 VNĐ',
            '~$2500',
            'Thỏa Thuận',
            '10.000.000 - 15.000.000 VNĐ',
            '20.000.000 - 30.000.000 VNĐ'
        ];

        for ($i = 1; $i <= 50; $i++) {
            $recruit = Recruit::query()->findOrNew($i);
            if ($recruit->exists) {
                continue;
            }

            $title = Arr::random($titles);
            $expiredAt = Carbon::today()->format('U') + rand(5, 30) * 86400;

            $recruit->fill([
                'title' => $title,
                'position' => $title,
                'body' => $this->getBody(),
                'work_type' => Arr::random($workTypes),
                'location' => Arr::random($locations),
                'pay_rate' => Arr::random($payRates),
                'specialisms' => implode(', ', Arr::random($specialisms, rand(1, 4))),
                'expired_at' => new Carbon($expiredAt),
            ]);
            $recruit->save();
        }
    }

    protected function getBody()
    {
        $contents = [
            '<div><p></p><p dir="ltr"><strong>Job Description:</strong></p><ul><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Research, analyze Blockchain projects;</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Work with BA to document the project;</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Prepare website content, products, pitch deck;</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Other tasks assigned by team leader.</p></li></ul><p dir="ltr"><strong>Requirements:</strong></p><ul><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Working full-time</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Having knowledge &amp; experience in content in Blockchain/Crypto</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Experience in writing project documents in English</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Ability to synthesize information and fluent in English (especially writing skills)</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Dynamic, analytical abilities.</p></li></ul><p dir="ltr"><strong>Benefits:</strong></p><ul><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Salary 10 - 20 million + bonus + join profitable bets</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Young, dynamic, professional working environment, promoting creativity and personal advantages.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Teambuilding, annual travel</p></li></ul><p dir="ltr"><strong>Working place</strong>: V6B - 12A Van Phu urban area, Ha Dong, Hanoi</p><ul><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Contact: <a href="mailto:people@techcom.io">people@techcom.io</a></p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Email subject: “<strong>CV Content writer - Full name</strong>”</p></li></ul><p>&nbsp;</p><p></p></div>',
            '<div><p></p><p dir="ltr"><strong>Job Description:</strong></p><ul><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Search and contact businesses that need to use Fintech and blockchain related financial services.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Coordinate with superiors to make public relations and develop business plans for Blockchain projects</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Manage and develop a plan to introduce and advise customers on products, maintain and develop customer relationships.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Participate in the product development process, build performance metrics, work with developers, engineers, and other members to plan development strategies - Coordinate with related departments such as planning team, customer support team to run and track work on schedule.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Manage, develop and maintain relationships with partners.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Make reports, manage work on JIRA tools</p></li></ul><p>&nbsp;</p><p dir="ltr"><strong>Requirements:</strong></p><ul><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">University graduate or higher in finance, economics, marketing, administration or equivalent majors.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Skills in communication, negotiation, persuasion, sales, good-looking appearance. Likes to learn new things, loves business, is not afraid to be rejected.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Positive working attitude, sense of responsibility, enthusiasm and initiative in work.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Have 1-2 years of experience in the field of Business Development</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Prior knowledge in Fintech, Blockchain, Crypto fields will be a good advantage - Proficient in Microsoft word, Excel, Email.Knowledge of Cryptocurrencies and Blockchain</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Good English negotiation skills (especially speaking ability)</p></li></ul><p dir="ltr">&nbsp;&nbsp;&nbsp;&nbsp;</p><p dir="ltr"><strong>Benefits:</strong></p><ul><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Income level: Negotiable according to capacity + Project bonus.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Have the opportunity to participate and build on big Blockchain projects.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Young, dynamic and comfortable working environment.</p></li><li dir="ltr" aria-level="1"><p dir="ltr" role="presentation">Annual travel, participate in extremely useful team building activities. - Working time: 8:30 am - 6:00 pm (Monday - Friday) and every other Saturday (lunch break 1:30 pm)</p></li></ul><p dir="ltr"><strong>Working place</strong>: V6B - 12A Van Phu urban area, Ha Dong, Hanoi</p><p dir="ltr">Contact: people@techcom.io</p><p style="text-align: justify;">&nbsp;</p><p></p></div>'
        ];

        return Arr::random($contents);
    }
}
