<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            FakeCategorySeeder::class,
            FakePostSeeder::class,
            FakeCompanyGroupSeeder::class,
            FakeCompanySeeder::class,
            FakePersonnelSeeder::class,
            FakeRecruitSeeder::class,
        ]);
    }
}
