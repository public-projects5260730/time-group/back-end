<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use Illuminate\Support\Str;

class FakeCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyGroups = [
            1 => [
                'Techcom Blockchain',
                'TimePay',
                'TimeBird',
                'TimeTrading',
                'NorthStudio',
                'ePlus',
                'IMS Blockchain',
                'Spinel',
                'KiwiGroup',
                'Bmoon Network',
                'CoinGo24',
            ],
            2 => [
                'TimeTech',
                'Spring AI',
                'Vilas Holdings',
            ],
            3 => [
                'TimeBeat',
                'Text Vision',
                'On Target Media',
                'Everyday_Epic',
            ],
            4 => [
                'TimeLaw'
            ]
        ];

        foreach ($companyGroups as $groupId => $companies) {
            foreach ($companies as $index => $name) {
                $result = Company::query()->firstOrNew(['name' => $name]);
                if ($result->exists) {
                    continue;
                }
                $logo = Str::slug($name);

                $result->fill([
                    'name' => $name,
                    'company_group_id' => $groupId,
                    'image' => "companies/logos/{$logo}.png",
                    'logo' => "companies/logos/{$logo}.png",
                    'address' => 'V6B-12A Văn phú, Hà đông, Hà Nội',
                    'email' => 'people@techcom.io',
                    'phone' => '0123 456 789',
                    'website' => 'www.techcom.io',
                    'brief_intro' => $this->companyBriefIntro(),
                    'personel' => 'Trần Tuấn Anh',
                    'position' => 'Giám Đốc',
                    'order' => ($index + 1) * 10
                ])->save();
            }
        }
    }

    protected function companyBriefIntro()
    {
        return '<div class="raw_components--panel--YDedw inspect_panels--inspectionPanel--o9CfO" tabindex="-1">
        <div>
        <div class="inspect_panels--copyableRow--ecLWm inspect_panels--highlightRow--BeZQJ">
        <div class="inspect_panels--propertyRowContent--qvupJ inspect_panels--copyableRow--ecLWm inspect_panels--highlightRow--BeZQJ">
        <div class="inspect_panels--contentProperty--McE5j text--fontPos11--rO47d text--_fontBase--VaHfk ellipsis--ellipsisAfter8Lines--2JTeB ellipsis--_ellipsisAfterNLines--5f-ox" dir="auto">Started since 2017, Techcom Blockchain is one of the pioneers in the field of developing Blockchain technology application solutions and crypto-related services. With the contribution of blockchain teams and experts, Techcom Blockchain brings customers effective solutions with the latest technology, big data security to reduce transaction costs, improve efficiency business and management Started since 2017, Techcom Blockchain is one of the pioneers in the field of developing Blockchain technology application solutions and crypto-related services. With the contribution of blockchain teams and experts, Techcom Blockchain brings customers effective solutions with the latest technology, big data security to reduce transaction costs, improve efficiency business and management</div>
        </div>
        </div>
        </div>
        </div>
        <div class="raw_components--panel--YDedw inspect_panels--inspectionPanel--o9CfO" tabindex="-1">
        <div class="inspect_panels--panelTitle--fJsa9 text--fontPos11--rO47d text--_fontBase--VaHfk">
        <div class="raw_components--panelTitle--gPkqQ inspect_panels--panelTitleText--Hx-lG ellipsis--ellipsis--70pHK">Typography</div>
        </div>
        </div>';
    }
}
