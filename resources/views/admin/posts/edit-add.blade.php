@php
    $edit = !is_null($dataTypeContent->getKey());
    $add = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::posts.edit-add')

@section('css')
    @parent

    <style>
        .seo-plugin .nav-tabs {
            border-bottom: 1px solid #dee2e6;
        }

        .seo-plugin .nav {
            display: flex;
            flex-wrap: wrap;
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }

        .seo-plugin .nav-tabs .nav-item.show .nav-link,
        .seo-plugin .nav-tabs .nav-link.active {
            color: #495057;
            background-color: #fff;
            border-color: #dee2e6 #dee2e6 #fff;
        }

        .seo-plugin .nav-tabs .nav-link {
            margin-bottom: -1px;
            background: 0 0;
            border: 1px solid transparent;
            border-top-left-radius: 0.25rem;
            border-top-right-radius: 0.25rem;
        }

        .seo-plugin .nav-link {
            display: block;
            padding: 0.5rem 1rem;
            color: #0d6efd;
            text-decoration: none;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out;
        }

        .seo-plugin .btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .seo-plugin .btn-group>.btn-group:not(:last-child)>.btn,
        .seo-plugin .btn-group>.btn:not(:last-child):not(.dropdown-toggle) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .seo-plugin .btn-group>.btn:first-child {
            margin-left: 0;
        }

        .seo-plugin .btn-group-vertical>.btn,
        .seo-plugin .btn-group>.btn {
            position: relative;
            flex: 1 1 auto;
        }

        .seo-plugin .btn-outline-secondary {
            color: #6c757d;
            border-color: #6c757d;
        }

        .seo-plugin .btn-outline-secondary:hover {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }

        .seo-plugin .collapse.show,
        .seo-plugin .tab-content .tab-pane {
            display: none !important;
        }

        .seo-plugin .collapse.in,
        .seo-plugin .tab-content .tab-pane.active {
            display: block !important;
            opacity: 1;
        }
    </style>
@stop
@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
            action="@if ($edit) {{ route('voyager.posts.update', $dataTypeContent->id) }}@else{{ route('voyager.posts.store') }} @endif"
            method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if ($edit)
                {{ method_field('PUT') }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <!-- ### TITLE ### -->
                    <div class="panel">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="voyager-character"></i> {{ __('voyager::post.title') }}
                                <span class="panel-desc"> {{ __('voyager::post.title_sub') }}</span>
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                    aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name' => 'title',
                                '_field_trans' => get_field_translations($dataTypeContent, 'title'),
                            ])
                            <input type="text" class="form-control" id="title" name="title"
                                placeholder="{{ __('voyager::generic.title') }}"
                                value="{{ $dataTypeContent->title ?? '' }}">
                        </div>
                    </div>

                    <!-- ### CONTENT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ __('voyager::post.content') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen"
                                    aria-hidden="true"></a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name' => 'body',
                                '_field_trans' => get_field_translations($dataTypeContent, 'body'),
                            ])
                            @php
                                $dataTypeRows = $dataType->{$edit ? 'editRows' : 'addRows'};
                                $row = $dataTypeRows->where('field', 'body')->first();
                            @endphp
                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                        </div>
                    </div><!-- .panel -->

                    <!-- ### EXCERPT ### -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{!! __('voyager::post.excerpt') !!}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                    aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @include('voyager::multilingual.input-hidden', [
                                '_field_name' => 'excerpt',
                                '_field_trans' => get_field_translations($dataTypeContent, 'excerpt'),
                            ])
                            <textarea class="form-control" name="excerpt">{{ $dataTypeContent->excerpt ?? '' }}</textarea>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ __('voyager::post.additional_fields') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                    aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @php
                                $dataTypeRows = $dataType->{$edit ? 'editRows' : 'addRows'};
                                $exclude = ['title', 'body', 'excerpt', 'slug', 'status', 'post_belongstomany_category_relationship', 'author_id', 'featured', 'image', 'meta_description', 'meta_keywords', 'seo_title'];
                            @endphp

                            @foreach ($dataTypeRows as $row)
                                @if (!in_array($row->field, $exclude))
                                    @php
                                        $display_options = $row->details->display ?? null;
                                    @endphp
                                    @if (isset($row->details->formfields_custom))
                                        @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                                    @else
                                        <div class="form-group @if ($row->type == 'hidden') hidden @endif @if (isset($display_options->width)) {{ 'col-md-' . $display_options->width }} @endif"
                                            @if (isset($display_options->id)) {{ "id=$display_options->id" }} @endif>
                                            {{ $row->slugify }}
                                            <label
                                                for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if ($row->type == 'relationship')
                                                @include('voyager::formfields.relationship', [
                                                    'options' => $row->details,
                                                ])
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                    aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="slug">{{ __('voyager::post.slug') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name' => 'slug',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'slug'),
                                ])
                                <input type="text" class="form-control" id="slug" name="slug" placeholder="slug"
                                    {!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, 'slug') !!} value="{{ $dataTypeContent->slug ?? '' }}">
                            </div>
                            <div class="form-group">
                                <label for="status">{{ __('voyager::post.status') }}</label>
                                <select class="form-control" name="status">
                                    <option value="PUBLISHED"@if (isset($dataTypeContent->status) && $dataTypeContent->status == 'PUBLISHED') selected="selected" @endif>
                                        {{ __('voyager::post.status_published') }}</option>
                                    <option value="DRAFT"@if (isset($dataTypeContent->status) && $dataTypeContent->status == 'DRAFT') selected="selected" @endif>
                                        {{ __('voyager::post.status_draft') }}</option>
                                    <option value="PENDING"@if (isset($dataTypeContent->status) && $dataTypeContent->status == 'PENDING') selected="selected" @endif>
                                        {{ __('voyager::post.status_pending') }}</option>
                                </select>
                            </div>

                            @php
                                $dataTypeRows = $dataType->{$edit ? 'editRows' : 'addRows'};
                                $include = ['post_belongstomany_category_relationship'];
                            @endphp

                            @foreach ($dataTypeRows as $row)
                                @if (in_array($row->field, $include))
                                    @php
                                        $display_options = $row->details->display ?? null;
                                    @endphp
                                    @if (isset($row->details->formfields_custom))
                                        @include('voyager::formfields.custom.' . $row->details->formfields_custom)
                                    @else
                                        <div class="form-group @if ($row->type == 'hidden') hidden @endif @if (isset($display_options->width)) {{ 'col-md-' . $display_options->width }} @endif"
                                            @if (isset($display_options->id)) {{ "id=$display_options->id" }} @endif>
                                            {{ $row->slugify }}
                                            <label
                                                for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if ($row->type == 'relationship')
                                                @include('voyager::formfields.relationship', [
                                                    'options' => $row->details,
                                                ])
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                            @endforeach

                            <div class="form-group">
                                <label for="featured">{{ __('voyager::generic.featured') }}</label>
                                <input type="checkbox"
                                    name="featured"@if (isset($dataTypeContent->featured) && $dataTypeContent->featured) checked="checked" @endif>
                            </div>
                        </div>
                    </div>

                    <!-- ### IMAGE ### -->
                    <div class="panel panel-bordered panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('voyager::post.image') }}</h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                    aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if (isset($dataTypeContent->image))
                                <img src="{{ filter_var($dataTypeContent->image, FILTER_VALIDATE_URL) ? $dataTypeContent->image : Voyager::image($dataTypeContent->image) }}"
                                    style="width:100%" />
                            @endif
                            <input type="file" name="image">
                        </div>
                    </div>

                    <!-- ### SEO CONTENT ### -->
                    <div class="panel panel-bordered panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="icon wb-search"></i> {{ __('voyager::post.seo_content') }}
                            </h3>
                            <div class="panel-actions">
                                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                    aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="meta_description">{{ __('voyager::post.meta_description') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name' => 'meta_description',
                                    '_field_trans' => get_field_translations(
                                        $dataTypeContent,
                                        'meta_description'),
                                ])
                                <textarea class="form-control" name="meta_description">{{ $dataTypeContent->meta_description ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="meta_keywords">{{ __('voyager::post.meta_keywords') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name' => 'meta_keywords',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords'),
                                ])
                                <textarea class="form-control" name="meta_keywords">{{ $dataTypeContent->meta_keywords ?? '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="seo_title">{{ __('voyager::post.seo_title') }}</label>
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name' => 'seo_title',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'seo_title'),
                                ])
                                <input type="text" class="form-control" name="seo_title" placeholder="SEO Title"
                                    value="{{ $dataTypeContent->seo_title ?? '' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="seo-plugin">
                @seoForm($dataTypeContent)
            </section>

        @section('submit-buttons')
            <button type="submit" class="btn btn-primary pull-right">
                @if ($edit)
                    {{ __('voyager::post.update') }}
                @else
                    <i class="icon wb-plus-circle"></i> {{ __('voyager::post.new') }}
                @endif
            </button>
        @stop
        @yield('submit-buttons')

        <input type="hidden" name="previousUrl" value="{{ url()->previous() }}">
    </form>

    <div style="display:none">
        <input type="hidden" id="upload_url" value="{{ route('voyager.upload') }}">
        <input type="hidden" id="upload_type_slug" value="{{ $dataType->slug }}">
    </div>
</div>

<div class="modal fade modal-danger" id="confirm_delete_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                </h4>
            </div>

            <div class="modal-body">
                <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                <button type="button" class="btn btn-danger"
                    id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- End Delete File Modal -->
@stop


@section('javascript')
@parent
<script>
    $('document').ready(function() {
        $('#collapseOne [data-toggle~="tab"]').on('click', function(e) {
            const $target = $(e.currentTarget);
            if ($target.hasClass('active')) {
                return;
            }
            $target.parent().find('[data-toggle~="tab"]').removeClass('active');
            $target.addClass('active');
        });

        $('.seo-plugin input[required]').removeAttr('required');
    });
</script>
@stop
