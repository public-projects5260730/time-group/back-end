<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Permission Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the permission broker for a permission update attempt
    | has failed, such as for an invalid token or invalid new permission.
    |
    */

    'do_not_have_permission' => 'You do not have permission to view this page or perform this action.',

];
