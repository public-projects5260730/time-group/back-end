<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('categories')->name('categories.')->group(function () {
    Route::match(['get', 'post'], '', 'CategoryController@index')->name('index');

    Route::prefix('{id}')->where(['id' => '[0-9]+'])->group(function () {
        Route::get('view', 'CategoryController@view')->name('view');
        Route::get('posts', 'CategoryController@posts')->name('posts');
    });
});

Route::prefix('posts')->name('posts.')->group(function () {
    Route::match(['get', 'post'], '', 'PostController@index')->name('index');

    Route::prefix('{slug}')->group(function () {
        Route::get('view', 'PostController@view')->name('view');
        Route::match(['get', 'post'], 'related', 'PostController@related')->name('related');
    });
});

Route::prefix('companies')->name('companies.')->group(function () {
    Route::get('', 'CompanyController@index')->name('index');
    Route::get('{id}/view', 'CompanyController@view')->whereNumber('id')->name('view');

    Route::get('groups', 'CompanyController@groups')->name('groups');
});

Route::prefix('banners')->name('banners.')->group(function () {
    Route::get('', 'BannerController@index')->name('index');
});

Route::prefix('personnels')->name('personnels.')->group(function () {
    Route::get('', 'PersonnelController@index')->name('index');
});

Route::prefix('recruits')->name('recruits.')->group(function () {
    Route::get('', 'RecruitController@index')->name('index');
    Route::get('options', 'RecruitController@options')->name('options');

    Route::prefix('{id}')->where(['id' => '[0-9]+'])->group(function () {
        Route::post('send-cv', 'RecruitController@sendCv');
    });
});

Route::prefix('contacts')->name('contacts.')->group(function () {
    Route::post('send-message', 'ContactController@sendMessage');
});

Route::prefix('settings')->name('settings.')->group(function () {
    Route::get('', 'SettingController@index')->name('index');
});

Route::prefix('seotags')->name('seotags.')->group(function () {
    Route::get('', 'SeoTagController@index')->name('index');
});
